package com.example.springsecurityjwt2.controller;

import com.example.springsecurityjwt2.dto.UpdateRolePermissionRequest;
import com.example.springsecurityjwt2.service.MyUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class UpdateRolePermissionController {

    private final MyUserService myUserService;

    @PutMapping("/give-access/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String updateRolePermission(@PathVariable Long id, @RequestBody UpdateRolePermissionRequest permissionRequest) {
        return myUserService.updateUserAccessRolePermission(id, permissionRequest);
    }
}
