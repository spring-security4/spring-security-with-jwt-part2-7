package com.example.springsecurityjwt2.controller;

import com.example.springsecurityjwt2.payload.request.LoginReuqest;
import com.example.springsecurityjwt2.payload.request.RegistreRequest;
import com.example.springsecurityjwt2.payload.response.JwtResponse;
import com.example.springsecurityjwt2.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/auth")
@Slf4j
public class AuthController {


    private final AuthService authService;

    /**
     * Login olduqda username ve password a gore bazada melumat yoxlanilib size jwt token verilecek
     */
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginReuqest loginReuqest) {
        long start = System.currentTimeMillis();
        JwtResponse jwtResponse = authService.login(loginReuqest);
        long end = System.currentTimeMillis();
        log.info("TOTAL TIME:" + (end - start));
        return ResponseEntity.ok(jwtResponse);
    }

    /**
     * Regisyter olunan zaman statik sekilde yazdigimiz role ve permissionlardan default ver access veriri yeni user e
     * ve admin oz isteyi ile istediyi usere access ve role verir
     * ve burda ola bilerki misal MANAGER roluna 8 access verdiyi halda diger user ada eyni rolu versede permissionlardan 4 u ne icaze versin
     * artiq burda funksionalliq qazandirmis oluruq
     *
     * Modelimiz ise beledir User i bir Authority id i olur Authority nin bir rolu ve coxlu permissionlari ola biler bu sayede
     * permisionlari istediyin usere isdeyin sayda vere bilersen
     */
    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody RegistreRequest request){
        return ResponseEntity.ok(authService.registerUser(request));
    }

}
