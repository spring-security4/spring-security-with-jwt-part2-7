package com.example.springsecurityjwt2.repository;

import com.example.springsecurityjwt2.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissioRepository extends JpaRepository<Permission,Integer> {
}
