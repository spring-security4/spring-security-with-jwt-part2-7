package com.example.springsecurityjwt2.service;

import com.example.springsecurityjwt2.jwt.JwtUtils;
import com.example.springsecurityjwt2.model.Authority;
import com.example.springsecurityjwt2.model.MyUser;
import com.example.springsecurityjwt2.model.Permission;
import com.example.springsecurityjwt2.model.Role;
import com.example.springsecurityjwt2.payload.request.LoginReuqest;
import com.example.springsecurityjwt2.payload.request.RegistreRequest;
import com.example.springsecurityjwt2.payload.response.JwtResponse;
import com.example.springsecurityjwt2.payload.response.RegisterResponse;
import com.example.springsecurityjwt2.repository.MyUserRepository;
import com.example.springsecurityjwt2.repository.PermissioRepository;
import com.example.springsecurityjwt2.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthService {

    private static final Integer ROLE_USER = 4;
    private static final Integer READ_TEST1 = 1;

    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final MyUserRepository myUserRepository;
    private final RoleRepository roleRepository;
    private final PermissioRepository permissioRepository;

    private final JwtUtils jwtUtils;

    public JwtResponse login(LoginReuqest loginReuqest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginReuqest.getUsername(), loginReuqest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtUtils.generateToken(authentication);
        return new JwtResponse(token);
    }

    public RegisterResponse registerUser(RegistreRequest request){
        MyUser myUser = new MyUser();
        myUser.setUsername(request.getUsername());
        myUser.setPassword(passwordEncoder.encode(request.getPassword()));
        myUser.setEmail(request.getEmail());
        myUser.setFirstName(request.getFirstName());
        myUser.setLastName(request.getLastName());
        myUser.setCreatedDate(LocalDateTime.now());
        myUser.setAuthority(defaultAuthority());
        myUserRepository.save(myUser);
        return new RegisterResponse("SUCCESS");
    }

    public Authority defaultAuthority() {
        Optional<Role> role = roleRepository.findById(ROLE_USER);
        Permission permissions = permissioRepository.getById(READ_TEST1);
        Authority authority = new Authority();
        authority.setRole(role.get());
        authority.setPermissions(List.of(permissions));
        return authority;
    }
}
