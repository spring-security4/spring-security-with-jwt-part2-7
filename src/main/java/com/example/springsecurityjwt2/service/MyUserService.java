package com.example.springsecurityjwt2.service;

import com.example.springsecurityjwt2.dto.UpdateRolePermissionRequest;
import com.example.springsecurityjwt2.model.MyUser;
import com.example.springsecurityjwt2.model.Permission;
import com.example.springsecurityjwt2.repository.MyUserRepository;
import com.example.springsecurityjwt2.repository.PermissioRepository;
import com.example.springsecurityjwt2.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MyUserService {

    private final MyUserRepository myUserRepository;
    private final RoleRepository roleRepository;
    private final PermissioRepository permissioRepository;

    public String updateUserAccessRolePermission(Long id, UpdateRolePermissionRequest permissionRequest) {
            MyUser myUser = myUserRepository.getById(id);
            myUser.getAuthority().setRole(roleRepository.getById(permissionRequest.getRoleId()));
            myUser.getAuthority().setPermissions(
                    Arrays.stream(permissionRequest.getPermissions()
                    ).map(permissioRepository::getById).collect(Collectors.toList()));
            myUserRepository.save(myUser);
            return "SUCCESS";

    }

}
